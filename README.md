# dubsmash

## Running

* Edit `.env.local` and supply `REXSTER_URL`
* Run tests: `docker-compose run smile python api/react_tests.py`
* That will also seed the graph
* Run `docker-compose up smile`
* Hit `$(docker-machine ip default):5000`

## API

### POST /reactions

Create a reaction.

#### Body:

```
{
  "dub": "<dubId>",
  "username": "<username>",
  "emoji": "<emoji :) :( etc>"
}
```

#### Response

* `201` if created
* `200` if already exists

### DELETE /reactions

Delete a reaction.

#### Body:

```
{
  "dub": "<dubId>",
  "username": "<username>",
  "emoji": "<emoji :) :( etc>"
}
```

#### Response

* `200` if deleted
* `5xx` if does not exist (we can gracefully handle this but leaving it for now)

### GET /dub/:dubId/reactions

Get hash of reactions for a given Dub ID.

## Challenges

* `graph.addVertex(T.label)` works, but `graph.addVertex(T.id)` doesn't. Don't know why, because the documents seem to imply this should work. But various trials did not work. That's why I'm finally using custom `username` and `id` fields for user and dub. Ideally I'd want to use the Vertex ID.
* Gremlin/Rexster seems to throw errors when do graph modification under `if` conditions. That's why I had to move toggling reactions into two calls. Definitely want to take a look at this more, and see why Gremlin throws errors in that case.
* Long time since Python :)
