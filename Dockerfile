FROM python:3

RUN wget -nv -O /usr/bin/dumb-init https://github.com/Yelp/dumb-init/releases/download/v1.0.0/dumb-init_1.0.0_amd64 && chmod +x /usr/bin/dumb-init

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/
RUN pip install -r requirements.txt

COPY . /myapp

EXPOSE 5000
ENTRYPOINT [ "dumb-init" ]
CMD ["python", "api/react.py"]
