from flask import json
import react
import unittest
from gremlin import flush

HEADERS = dict({ 'content-type':'application/json' })

class AppTestCase(unittest.TestCase):
  def setUp(self):
    self.app = react.app.test_client()
    flush()

  def get_reaction(self, dub):
    rv = self.app.get('/dub/%s/reactions' % dub, headers=HEADERS)
    return json.loads(rv.data)

  def test_empty_reaction(self):
    reactions = self.get_reaction('dub2')
    self.assertEqual(reactions, dict())

  def test_create_reaction(self):
    payload = json.dumps(dict(dub='dub3', username='tim', emoji=':D'))
    rv = self.app.post('/reactions', data=payload, headers=HEADERS)
    self.assertEqual(rv.status_code, 201)
    reactions = self.get_reaction('dub3')
    self.assertEqual(reactions, {':D':1 })

  def test_create_many_reactions(self):
    payloads = [
      json.dumps(dict(dub='dub3', username='daniel', emoji=':p')),
      json.dumps(dict(dub='dub3', username='tim', emoji=':D')),
      json.dumps(dict(dub='dub3', username='subhas', emoji=':D')),
      json.dumps(dict(dub='dub3', username='moritz', emoji=':D'))
    ]
    for payload in payloads:
      self.app.post('/reactions', data=payload, headers=HEADERS)
    reactions = self.get_reaction('dub3')
    self.assertEqual(reactions, {':p':1, ':D': 3 })

  def test_update_reaction(self):
    payload = json.dumps(dict(dub='dub4', username='daniel', emoji=':p'))
    self.app.post('/reactions', data=payload, headers=HEADERS)
    rv = self.app.post('/reactions', data=payload, headers=HEADERS)
    self.assertEqual(rv.status_code, 200)
    reactions = self.get_reaction('dub4')
    self.assertEqual(reactions, {':p':1 })

  def test_delete_reaction(self):
    payload = json.dumps(dict(dub='dub2', username='moritz', emoji=':o'))
    self.app.post('/reactions', data=payload, headers=HEADERS)
    rv = self.app.delete('/reactions', data=payload, headers=HEADERS)
    self.assertEqual(rv.status_code, 200)
    reactions = self.get_reaction('dub2')
    self.assertEqual(reactions, dict())

if __name__ == '__main__':
  unittest.main()
