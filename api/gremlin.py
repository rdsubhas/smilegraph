import requests
import os
import json
from textwrap import dedent

REXSTER_URL = os.environ['REXSTER_URL']
SESSION = requests.Session()

# poor man's wrapper around rexster
def rexec(cmd):
  payload = json.dumps({ "gremlin": cmd })
  res = SESSION.post(REXSTER_URL, data=payload)
  res.raise_for_status()
  return res.json()

def flush():
  cmd1 = 'g.E().drop(); g.V().drop();'
  cmd2 = dedent('''
    user1 = graph.addVertex(T.label, 'user', 'username', 'subhas');
    user2 = graph.addVertex(T.label, 'user', 'username', 'tim');
    user3 = graph.addVertex(T.label, 'user', 'username', 'moritz');
    user4 = graph.addVertex(T.label, 'user', 'username', 'daniel');
    dub1 = graph.addVertex(T.label, 'dub', 'id', 'dub1');
    dub2 = graph.addVertex(T.label, 'dub', 'id', 'dub2');
    dub3 = graph.addVertex(T.label, 'dub', 'id', 'dub3');
    dub4 = graph.addVertex(T.label, 'dub', 'id', 'dub4');
    edge1 = user1.addEdge('reacted', dub1, 'emoji', ':)');
''')
  rexec(cmd1)
  rexec(cmd2)
