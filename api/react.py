import os
from gremlin import rexec, flush
from textwrap import dedent
from flask import Flask, jsonify, request

ENVIRONMENT = os.environ['ENVIRONMENT']
app = Flask(__name__)

@app.route('/dub/<dubId>/reactions', methods=['GET'])
def get_reactions(dubId):
  cmd = "g.V().hasLabel('dub').has('id', '%s').inE('reacted').groupCount().by('emoji')" % dubId
  data = rexec(cmd).get('result').get('data')[0]
  return jsonify(data)

@app.route('/reactions', methods=['POST'])
def create_reaction():
  params = request.get_json()
  cmd = dedent('''
    user = g.V().hasLabel('user').has('username', '%(username)s');
    reaction = user.outE('reacted').has('emoji', '%(emoji)s').inV().has('id', '%(dub)s');
    reaction.hasNext();
''' % params)
  try:
    exists = rexec(cmd).get('result').get('data')[0]
  except Exception:
    exists = False

  if not exists:
    cmd = dedent('''
      user = g.V().hasLabel('user').has('username', '%(username)s').next();
      dub = g.V().hasLabel('dub').has('id', '%(dub)s').next();
      user.addEdge('reacted', dub, 'emoji', '%(emoji)s');
''' % params)
    rexec(cmd)
    return '', 201
  else:
    return '', 200

@app.route('/reactions', methods=['DELETE'])
def delete_reaction():
  cmd = dedent('''
    edge = g.V().hasLabel('user').has('username', '%(username)s')
      .outE('reacted').has('emoji', '%(emoji)s').as('edge')
      .inV().hasLabel('dub').has('id', '%(dub)s')
      .select('edge').next().remove();
''' % request.get_json())
  data = rexec(cmd)
  return '', 200

if __name__ == '__main__':
  app.run(host='0.0.0.0', debug=(ENVIRONMENT != 'production'))
